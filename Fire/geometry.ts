namespace geometry {
    export class point {
        x:number;
        y:number;
        constructor(x:number,y:number)
        {
            this.x =x;
            this.y = y;
        }
    }
    export class circle {
        private alpha:number;
        private radius:number;
        private P:point;
        private vx:number;
        private vy:number;
        private canvas:HTMLCanvasElement;
        private context:CanvasRenderingContext2D;
        public color:string;
        constructor (canvas:HTMLCanvasElement,radius:number,vx:number,vy:number,P:point) {
            this.radius = radius;
            this.vx=vx;
            this.vy = vy;
            this.P = new point(P.x,P.y);
            this.context = this.canvas.getContext('2d');
            this.alpha = 1;
            this.color = "yellow";
        }
        draw() {
            this.context.save();
            this.context.beginPath()
            this.context.arc(this.P.x,this.P.y,this.radius,0,2*Math.PI);
            this.context.fillStyle = this.color;
            this.context.globalAlpha = this.alpha;
            this.context.fill();
            this.context.restore();
            this.update();
            
        }
        private update() {
            this.P.x = this.P.x + this.vx;
            this.P.y = this.P.y + this.vy;
            this.alpha *=0.98;
        }
        get Alpha() {
            return this.alpha;
        }

    }
    class fire{
        private circlecontainer: {circlr:circle}[];
        private x:number;
        private y:number;
        private canvas:HTMLCanvasElement;
        private context:CanvasRenderingContext2D;
        private no:number;
        public velocityrange
    }
}
