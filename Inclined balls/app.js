var canvas = document.getElementById("mycanvas");
var context = canvas.getContext('2d');
canvas.addEventListener("click", mouseclick, false);
var rect = canvas.getBoundingClientRect();
var userpoint;
var scene = new geometry.scene(canvas);
var ismoving = false;
var i = 0;
function mouseclick(e) {
    let x = e.clientX - rect.x;
    let y = e.clientY - rect.y;
    userpoint = new geometry.point(x, y);
    var radius = parseFloat(document.getElementById("radius").value);
    if (i == 1 && !isNaN(radius)) {
        var circle = new geometry.circle(context, userpoint, radius);
        scene.add(circle); //Add a circle in container
    }
    scene.drawall();
}
function animate() {
    if (i == 2) {
        scene.animate();
        window.requestAnimationFrame(animate);
    }
}
function draw() {
    i = 1;
    ismoving = false;
}
function move() {
    i = 2;
    if (!ismoving) {
        ismoving = true;
        animate();
    }
}
//# sourceMappingURL=app.js.map