var geometry;
(function (geometry_1) {
    class point {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
    }
    geometry_1.point = point;
    class circle {
        constructor(context, centre, radius) {
            this.forwardmotion = true;
            this._color = "silver";
            this.centre = centre;
            this.context = context;
            this.radius = radius;
        }
        draw() {
            this.context.beginPath();
            this.context.arc(this.centre.x, this.centre.y, this.radius, 0, 2 * Math.PI);
            this.context.fillStyle = this._color;
            this.context.fill();
            this.vx = this.random(1, 8);
            this.vy = this.random(1, 8);
        }
        set color(color) {
            this._color = color;
        }
        isInside(pt) {
            let r = Math.sqrt(Math.pow((pt.x - this.centre.x), 2) + Math.pow((pt.y - this.centre.y), 2));
            if (r < this.radius) {
                return true;
            }
            return false;
        }
        random(minval, maxval) {
            return (Math.random() * (maxval - minval) + minval);
        }
        get centre_point() {
            return (this.centre);
        }
    }
    geometry_1.circle = circle;
    class scene {
        constructor(canvas) {
            this.canvas = canvas;
            this.geometryContainer = [];
            this.startpoint = new point(0, 0);
            this.endpoint = new point(this.canvas.width, this.canvas.height);
            this.mycolors = ["Red", "Green", "Blue", "Black", "Yellow", "Orange", "Pink", "Brown", "Grey", "Violet", "Indigo", "Magenta", "Gold", "Silver", "#E56C7C", "#F52A4B"];
        }
        add(geometry) {
            this.geometryContainer.push(geometry);
        }
        drawall() {
            var context = this.canvas.getContext('2d');
            context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            for (let i = 0; i < this.geometryContainer.length; i++) {
                this.geometryContainer[i].draw();
            }
        }
        changecolor(color, usrpt) {
            for (var i = 0; i < this.geometryContainer.length; i++) {
                if (this.geometryContainer[i].isInside(usrpt)) //if user point is inside myshape
                 {
                    this.geometryContainer[i].color = color;
                    break;
                }
            }
        }
        toandfro() {
            for (var i = 0; i < this.geometryContainer.length; i++) {
                if (this.geometryContainer[i].forwardmotion) {
                    this.geometryContainer[i].centre.x += this.geometryContainer[i].vx;
                    this.geometryContainer[i].centre.y += this.geometryContainer[i].vy;
                    // alert(this.geometryContainer[i].vx);
                    this.geometryContainer[i].color = this.mycolors[Math.floor(Math.random() * this.mycolors.length)];
                    if ((this.geometryContainer[i].centre.x >= this.endpoint.x - this.geometryContainer[i].radius) || (this.geometryContainer[i].centre.y >= this.endpoint.y - this.geometryContainer[i].radius)) {
                        this.geometryContainer[i].forwardmotion = false;
                    }
                }
                else {
                    this.geometryContainer[i].centre.x -= this.geometryContainer[i].vx;
                    this.geometryContainer[i].centre.y -= this.geometryContainer[i].vy;
                    this.geometryContainer[i].color = this.mycolors[Math.floor(Math.random() * this.mycolors.length)];
                    if ((this.geometryContainer[i].centre.x <= this.startpoint.x + this.geometryContainer[i].radius) || this.geometryContainer[i].centre.y <= this.startpoint.y + this.geometryContainer[i].radius) {
                        this.geometryContainer[i].forwardmotion = true;
                        // alert(this.geometryContainer[i].forwardmotion);
                    }
                }
            }
        }
        animate() {
            this.toandfro();
            this.drawall();
        }
    }
    geometry_1.scene = scene;
})(geometry || (geometry = {}));
//# sourceMappingURL=geometry.js.map