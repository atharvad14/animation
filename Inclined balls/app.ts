var canvas: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context: CanvasRenderingContext2D = canvas.getContext('2d');
canvas.addEventListener("click", mouseclick, false);
var rect = canvas.getBoundingClientRect();
var userpoint: geometry.point;
var scene: geometry.scene = new geometry.scene(canvas);
var ismoving : boolean = false;
var i: number = 0;

function mouseclick(e: MouseEvent) {
    let x = e.clientX - rect.x;
    let y = e.clientY - rect.y;
    userpoint = new geometry.point(x, y);
    var radius: number = parseFloat((<HTMLInputElement>document.getElementById("radius")).value);
    if (i == 1 && !isNaN(radius)) {
        var circle = new geometry.circle(context, userpoint, radius);
        scene.add(circle);      //Add a circle in container
    }
    
    scene.drawall();
}

function animate()
{
    if (i==2)
    {
        scene.animate();
        window.requestAnimationFrame(animate);
    }
}
function draw() {
    i = 1;
    ismoving = false;
}
function move() {

    i = 2;
    if (!ismoving)
    {
        ismoving = true;
        animate();
    }
}